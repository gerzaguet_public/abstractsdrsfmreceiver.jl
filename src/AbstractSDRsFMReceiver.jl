module AbstractSDRsFMReceiver

using AbstractSDRs;
using DSP; 
using FFTW;

include("Audio.jl");
using .Audio


function fmDemod(sig)
    out = zeros(Float64,length(sig));
    @inbounds @simd for n ∈ (1:length(sig)-1)
        out[n+1] = angle(sig[n+1]*conj(sig[n]));
    end
    return out;
end

function genFilter(sizeL,bandPass,samplingRate)
    # --- Init frequency response 
    cuttOff = bandPass / samplingRate;
    fRep  = zeros(Complex{Float64},sizeL);
    # --- Set passband value 
    fRep[1:Int(floor(sizeL*cuttOff))] .= 1;
    # --- Adding linear phase term
    pulsation   = 2*pi*(0:sizeL-1) ./sizeL;# --- Pulsation \Omega 
    groupDelay  = - (sizeL-1)/2;# --- Group delay 
    fRep        .= fRep .* exp.(1im * groupDelay .* pulsation);
    # --- Get to time domain
    # With a force to cast in real domain 
    hRep  = real(ifft(fRep));
    # --- Generates window 
    wind  = hamming(sizeL);
    # --- Apply windowing (apodisation)
    h  = hRep .* wind;
end 

function applyFilter(x,h,decim)
    l = length(h)÷2;
    return conv(x,h)[1+l:decim:end-l];
end

function main(sdr,carrierFreq;kwargs...)
    # --- Simulation parameters
    global newCarrierFreq	  = carrierFreq;		# --- Starting frequency
    global newGain		      = 25;			  		# --- Analog Rx gain
    samplingRate  			  = 168e3;
    # --- Duration and buffer size 
    duration                  = 3;
    nbSamples                 = Int( duration * samplingRate);
    # --- Update radio configuration
    global radio			= openSDR(sdr,newCarrierFreq,samplingRate,newGain;kwargs...); 
    # --- Create FIR filter 
    audioRendering = 44e3;
    decim          = Int(samplingRate ÷ audioRendering);
    h              = genFilter(128,48e3,samplingRate);
    # --- Audio rendering 
    stream = Audio.createAudioBuffer();
    try 
        while(true)
            # --- We get buffer 
            sig = recv(radio,nbSamples);
            # --- FM demodulator
            out = fmDemod(sig);
            # --- Audio decimation 
            audio = applyFilter(out,h,decim);
            # --- Audio rendering 
            Audio.play(stream,audio)
        end
    catch exception;
        close(stream);
        close(radio);
        rethrow(exception);
    end 
    close(stream);
    close(radio);
end

    end # module
